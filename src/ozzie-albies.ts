import {Client, Message, RichEmbed, TextChannel} from "discord.js";
import {getBotSetting, setBotSetting} from "./database";
import {
    DEVELOPMENT_MODE,
    SNOWFLAKE_CHANNEL_LLAMOS_TESTING,
    SNOWFLAKE_CHANNEL_OAC_SCOREBOARD,
    SNOWFLAKE_SERVER_MLR_DEVELOPMENT,
    SNOWFLAKE_SERVER_OZZIE_ALBIES,
} from "./important-constants";
import {getPlayerWithStats} from "./site-pull";
import {Player} from "./site-types";

let discordClient: Client;
let initialized: boolean = false;
export async function init(client: Client) {
    initialized = true;
    discordClient = client;
    updateOACScoreboard()
        .catch(console.error);
    setInterval(() => updateOACScoreboard().catch(console.error), 5 * 60 * 1000);
}

const PLAYERS = [
    541, // Dookie Stane
    518, // Arthur Vandelay
    492, // Wallace Grommet
    553, // Joshua Jones
    488, // Juniped Catfish
    485, // Francois Onime
    501, // Fuzzy Spit
    476, // Eddie Swingstyle
    638, // Sablo Panchez
];

class PlayerWithOPS {
    name: string;
    ops: number;
}

async function updateOACScoreboard() {
    let players: PlayerWithOPS[] = [];
    for (let id of PLAYERS) {
        let player = await getPlayerWithStats(id, 4, false) as Player;
        let hits = player.battingStats.totalHr + player.battingStats.total3b + player.battingStats.total2b + player.battingStats.total1b;
        let ob = hits + player.battingStats.totalBb;
        let obp = player.battingStats.totalPas !== 0 ? ob / player.battingStats.totalPas : 0;
        let slg = player.battingStats.totalAbs !== 0 ? (4 * player.battingStats.totalHr + 3 * player.battingStats.total3b + 2 * player.battingStats.total2b + player.battingStats.total1b) / player.battingStats.totalAbs : 0;
        let ops = obp + slg;
        players.push({name: player.name, ops});
    }

    let fields: Array<{name: string, value: string, inline: boolean}> = [];
    for (let player of players.sort((a, b) => b.ops - a.ops)) {
        fields.push({name: player.name, value: player.ops.toFixed(3), inline: false});
    }

    let embed = new RichEmbed({
        title: "Ozzie Albie's Scoreboard",
        footer: {text: "Updated Automatically"},
        timestamp: new Date(),
        fields,
    });

    let message = await getBotSetting("oac.message");
    let guild = discordClient.guilds.get(DEVELOPMENT_MODE ? SNOWFLAKE_SERVER_MLR_DEVELOPMENT : SNOWFLAKE_SERVER_OZZIE_ALBIES);
    let channel = guild.channels.get(DEVELOPMENT_MODE ? SNOWFLAKE_CHANNEL_LLAMOS_TESTING : SNOWFLAKE_CHANNEL_OAC_SCOREBOARD) as TextChannel;
    if (message !== undefined) {
        let existingMessage: Message;
        try {
            existingMessage = await channel.fetchMessage(message);
        } catch (e) {
            console.log("Couldn't find scoreboard message, recreating....");
        }
        if (existingMessage !== undefined) {
            await existingMessage.edit("", embed);
            return;
        }
    }

    let createdMessage: Message = await channel.send(embed) as Message;
    await setBotSetting("oac.message", createdMessage.id);
}
