import {Snowflake} from "discord.js";
import {readFileSync} from "fs";
import * as mysql from "promise-mysql";
import {DEVELOPMENT_MODE, PlayerSetting} from "./important-constants";
import {getPlayerByRedditName, Player, Team} from "./players";

let dbConfig = JSON.parse(readFileSync("database.json", "utf8"));
let currentProfile = DEVELOPMENT_MODE ? "dev" : "prod";

/* ts-lint:disable:no-string-literal */
const pool = mysql.createPool({
    connectionLimit: 10,
    host: dbConfig[currentProfile].host,
    user: dbConfig[currentProfile].user,
    password: dbConfig[currentProfile].password,
    database: dbConfig[currentProfile].database,
    ssl: {rejectUnauthorized: false},
});

/* ts-lint:enable */

/**
 * Finds a Discord snowflake by looking up a sheet-player
 * @param player The player to search
 * @return The snowflake found, or undefined
 */
export async function getSnowflake(player: Player): Promise<string | undefined> {
    // Search for snowflake by reddit name
    let res = await pool.query("SELECT person_snowflake FROM people WHERE reddit_name=?", [player.redditName]);
    if (res[0])
        return res[0].person_snowflake;

    // If none found, return undefined
    return undefined;
}

/**
 * Searches for a sheet-player by looking up a Discord snowflake
 * @param snowflake the snowflake to search
 * @return The player if found, or undefined
 */
export async function getPlayer(snowflake: Snowflake): Promise<Player | undefined> {
    // Search for reddit name by snowflake, then find the player off the sheet with that
    let res = await pool.query("SELECT reddit_name FROM people WHERE person_snowflake=?", [snowflake]);
    if (res[0])
        return await getPlayerByRedditName(res[0].reddit_name);

    // If we couldn't find the snowflake, return undefined
    return undefined;
}

export async function saveGameThread(away: Team, home: Team, id36: string) {
    if (id36.length > 7) {
        return "ID36 length does not match.";
    }
    await pool.query("INSERT INTO games (away_team, home_team, id36) VALUES (?, ?, ?)", [away.tag, home.tag, id36]);
    console.log(`Saved game thread ${away.tag}@${home.tag} -> ${id36}`);
    return undefined;
}

// Get all information for a team's games. Optional parameter at 1 just in case you only need the latest game.
export async function getLatestGameThreadsForTeam(team: Team, numberOfResults = 1): Promise<any[]> {
    let res = await pool.query("SELECT * FROM games WHERE home_team = ? OR away_team = ? ORDER BY created DESC LIMIT ?", [team.tag, team.tag, numberOfResults]);
    return res ? res : [];
}

/**
 * This method grabs a boolean value from the DB's player settings.
 * It is an arbitrary key-value store, with each setting being unique and independent.
 * @param user The user to get a setting value of.
 * @param setting The setting to get. Max length of 128.
 * @param defaultValue The default value to return if the setting is not defined in the DB
 */
export async function getUserSetting(user: Snowflake, setting: PlayerSetting, defaultValue?: boolean): Promise<boolean | undefined> {
    let res = await pool.query("SELECT setting_value FROM settings WHERE setting_user=? AND setting_key=?", [user, setting]);
    if (res[0] !== undefined)
        return res[0].setting_value;

    return defaultValue;
}

/**
 * This method stores a boolean value in the DB's player settings.
 * It is an arbitrary key-value store, with each setting being unique and independent.
 * This method will overwrite settings that already exist.
 * @param user The user to store a setting for.
 * @param setting The setting name to set. Max length of 128.
 * @param newValue The new value of the setting.
 */
export async function setUserSetting(user: Snowflake, setting: PlayerSetting, newValue: boolean) {
    await pool.query("INSERT INTO settings (setting_user, setting_key, setting_value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE setting_value=?", [user, setting, newValue, newValue]);
    console.log(`Updated player setting ${setting} for ${user} to ${newValue}`);
}

export async function setBotSetting(settingKey: string, settingValue: string) {
    await pool.query(`INSERT INTO bot_settings (setting_name, setting_value) VALUES (?, ?) ON DUPLICATE KEY UPDATE setting_value=?`, [settingKey, settingValue, settingValue]);
    console.log(`Set bot setting "${settingKey} => ${settingValue}"`);
}

export async function getBotSetting(settingKey: string): Promise<string | undefined> {
    let res = await pool.query(`SELECT setting_value
                                FROM bot_settings
                                WHERE setting_name = ?`, [settingKey]);
    if (res[0])
        return res[0].setting_value;

    return undefined;
}

export async function addComplaint(author: Snowflake, content: string): Promise<number> {
    let res = await pool.query(`INSERT INTO complaints (author, content) values (?, ?)`, [author, content]);
    return res.insertId;
}

export async function appendComplaint(complaint: number, content: string) {
    await pool.query(`UPDATE complaints SET content = CONCAT(content, '\n\n', ?) where id = ?`, [content, complaint]);
}

export async function getComplaintAuthor(complaint: number): Promise<string> {
    let res = await pool.query(`SELECT author FROM complaints WHERE id = ?`, [complaint]);
    if (res[0])
        return res[0].author;

    return undefined;
}

// endregion
