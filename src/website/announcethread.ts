import {Guild, TextChannel} from "discord.js";
import {SNOWFLAKE_CHANNEL_GAME_DISCUSSION} from "../important-constants";

export async function handleThreadAnnounce(guild: Guild, id36: string, awayTeamId: string, homeTeamId: string, umps: string[]) {

    await (guild.channels.get(SNOWFLAKE_CHANNEL_GAME_DISCUSSION) as TextChannel).send(
        `<@&${awayTeamId}> <@&${homeTeamId}>, your game thread has been created!\n\n` +
        `Your umps are: ${umps.map(u => `<@${u}>`).join(", ")}\n\n` +
        `https://reddit.com/r/fakebaseball/comments/${id36}`,
    );

}
