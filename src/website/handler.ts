import {Message} from "discord.js";
import {handleThreadAnnounce} from "./announcethread";
import {handleSiteABPingRequest} from "./ping";
import { handleSiteRoleRequest } from "./role";

export async function handleSiteMessage(msg: Message) {
    let args = msg.content.split("|");
    let command = args.shift();

    switch (command) {
        case "ABPING":
            await handleSiteABPingRequest(msg, args[0], args[1], args[2] === "true", args[3], args[4].split(" "));
            break;
        case "ROLE":
            await handleSiteRoleRequest(msg.guild, args[0], args[1], args[2] === "true");
            break;
        case "ANNOUNCETHREAD":
            await handleThreadAnnounce(msg.guild, args[0], args[1], args[2], args[3].split(" "));
            break;
    }
}
