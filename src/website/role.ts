import {Guild, Snowflake} from "discord.js";
import {createNewABPing} from "../ab-ping-orchestrator";
import {
    SNOWFLAKE_CHANNEL_AB_PINGS,
    SNOWFLAKE_CHANNEL_UMP_PINGS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
} from "../important-constants";

export async function handleSiteRoleRequest(guild: Guild, user: Snowflake, role: Snowflake, derole: boolean) {
    // Find the user in the server
    let member = await guild.fetchMember(user, true);

    // If they aren't in main, fail silently
    if (!member)
        return;

    // Add/remove the role
    if (derole)
        await member.removeRole(role);
    else
        await member.addRole(role);
}
