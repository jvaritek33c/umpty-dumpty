import {Snowflake} from "discord.js";

export const SNOWFLAKE_USER_LLAMOS: Snowflake = "138318226181324800";
export const SNOWFLAKE_ROLE_COMMISSIONER: Snowflake = "583740933389156364";
export const SNOWFLAKE_ROLE_TECH: Snowflake = "583741086544166948";
export const SNOWFLAKE_ROLE_GM: Snowflake = "344859708022194176";
export const SNOWFLAKE_ROLE_UMP: Snowflake = "451060214477881355";
export const SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN: Snowflake = "344859525582422016";
export const SNOWFLAKE_SERVER_MLR_DEVELOPMENT: Snowflake = "567875961761300522";
export const SNOWFLAKE_SERVER_OZZIE_ALBIES: Snowflake = "598316040341946369";
export const SNOWFLAKE_CHANNEL_UMP_PINGS: Snowflake = "573242415671017493";
export const SNOWFLAKE_CHANNEL_AB_PINGS: Snowflake = "572841324391170059";
export const SNOWFLAKE_CHANNEL_BOT_COMMANDS: Snowflake = "429974561484701706";
export const SNOWFLAKE_CHANNEL_DRAFT: Snowflake = "593178941137616937";
export const SNOWFLAKE_CHANNEL_OOTC: Snowflake = "597114169514393610";
export const SNOWFLAKE_CHANNEL_NL_CENTRAL: Snowflake = "547143097931268096";
export const SNOWFLAKE_CHANNEL_SCOREBOARD: Snowflake = "586319854290862080";
export const SNOWFLAKE_CHANNEL_PROPOSAL_VOTES: Snowflake = "509808822718824450";
export const SNOWFLAKE_CHANNEL_SITE_COMMUNICATION: Snowflake = "611545843836649486";
export const SNOWFLAKE_CHANNEL_LLAMOS_TESTING: Snowflake = "568522191046705153";
export const SNOWFLAKE_CHANNEL_OAC_SCOREBOARD: Snowflake = "604336215143219213";
export const SNOWFLAKE_CHANNEL_GAME_DISCUSSION: Snowflake = "344859525582422017";
export const SNOWFLAKE_CHANNEL_COMPLAINTS: Snowflake = "651483758565785606";
export const SNOWFLAKE_ROLE_PLAYER: Snowflake = "490008203925389344";

export const DEVELOPMENT_MODE: boolean = parseInt(process.env.DEVELOPMENT) === 1;

export const REDDIT_ID36_FROM_URL_REGEX = /https:?\/\/.*?\/r\/fakebaseball\/comments\/([0-9a-z]{6,7}).*/i;

export const EMBED_FOOTER = {
    icon_url: "https://cdn.discordapp.com/avatars/138318226181324800/a_ee07f391bf1b75ee4c91bc7878fb838f.png",
    text: "If something seems wrong, please message Llamos#0001.",
};

export enum PlayerSetting {
    PingOptIn = "ping.optin",
    PingOptInGM = "ping.gmoptin",
    PingOptInUmp = "ping.umpoptin",
    RangesCompact = "ranges.compact",
}
