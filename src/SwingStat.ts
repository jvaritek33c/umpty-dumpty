import * as rp from "request-promise";
import {Park, Player} from "./site-types";

export class Named {
    name: string;
}
/**
 * Represents a set of numbers used in swing calculation.
 * Each outcome is assigned a range, and is combined linearly.
 */
export class SwingStat extends Named {
    rangeHR: number;
    range3B: number;
    range2B: number;
    range1B: number;
    rangeBB: number;
    rangeFO: number;
    rangeK: number;
    rangePO: number;
    rangeRGO: number;
    rangeLGO: number;

    constructor(name: string, rangeHR: number, range3B: number, range2B: number, range1B: number, rangeBB: number, rangeFO: number, rangeK: number, rangePO: number, rangeRGO: number, rangeLGO: number) {
        super();
        this.name = name;
        this.rangeHR = rangeHR;
        this.range3B = range3B;
        this.range2B = range2B;
        this.range1B = range1B;
        this.rangeBB = rangeBB;
        this.rangeFO = rangeFO;
        this.rangeK = rangeK;
        this.rangePO = rangePO;
        this.rangeRGO = rangeRGO;
        this.rangeLGO = rangeLGO;
    }

    /**
     * Generates a table-like output of the SwingStat
     * with each column padded to length 4
     * and delimited by vertical pipes (|).
     */
    toString() {
        return `|${this.rangeHR.toString().padEnd(4, " ")}|${this.range3B.toString().padEnd(4, " ")}|${this.range2B.toString().padEnd(4, " ")}|${this.range1B.toString().padEnd(4, " ")}|${this.rangeBB.toString().padEnd(4, " ")}|${this.rangeFO.toString().padEnd(4, " ")}|${this.rangeK.toString().padEnd(4, " ")}|${this.rangePO.toString().padEnd(4, " ")}|${this.rangeRGO.toString().padEnd(4, " ")}|${this.rangeLGO.toString().padEnd(4, " ")}|`;
    }
}

// These 3 have no special behaviour
export class BattingType extends SwingStat {}
export class PitchingBonus extends SwingStat {}
export class PitchingType extends SwingStat {}

export class Field extends SwingStat {

    constructor(name: string, rangeHR: number, range3B: number, range2B: number, range1B: number, rangeBB: number) {
        super(name, rangeHR, range3B, range2B, range1B, rangeBB, 0, 0, 0, 0, 0);
    }
}

/**
 * Calculates the ranges of a batter against a pitcher in a given field.
 * @param batter The batter player.
 * @param pitcher The pitcher player.
 * @param park The field.
 * @param infieldIn Whether or not the infield is in
 */
export async function calculateRanges(batter: Player, pitcher: Player, park: Park, infieldIn: boolean = false): Promise<SwingStat> {
    // Use the web endpoint
    return (await rp(`https://redditball.com/api/v2/calculator/ranges?pitcher=${pitcher.id}&batter=${batter.id}&park=${park.id}&infieldIn=${infieldIn}`, {json: true})).range as SwingStat;
}
