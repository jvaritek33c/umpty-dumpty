import {Message, RichEmbed} from "discord.js";
import * as Discord from "discord.js";
import {DEVELOPMENT_MODE, EMBED_FOOTER, SNOWFLAKE_CHANNEL_DRAFT, SNOWFLAKE_CHANNEL_OOTC} from "../important-constants";
import {getPlayer as getPlayerFromSite, getTeam} from "../site-pull";
import {Team} from "../site-types";
import {Command} from "./command";

let draftTimerMessage: Discord.Message;
let draftTimerTeam: Team;
let draftTimerEnd: number;
let draftTimerEmoji: Discord.Emoji;

class DraftCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let userAuthorized = DEVELOPMENT_MODE || msg.channel.id === SNOWFLAKE_CHANNEL_OOTC;
        if (!userAuthorized)
            return `You can't do that.`;

        // Lookup the references
        let team = await getTeam(args.shift());
        let player = await getPlayerFromSite(args.join(" "));

        if (draftTimerMessage) {
            await draftTimerMessage.delete();
            draftTimerMessage = undefined;
            draftTimerEnd = undefined;
            draftTimerTeam = undefined;
            draftTimerEmoji = undefined;
        }

        // Make sure they're valid
        if (typeof team === "string")
            return team;
        if (typeof player === "string")
            return player;

        // Generate the embed
        let emoji = msg.guild.emojis.find(e => e.name.toLowerCase() === (team as Team).tag.toLowerCase());
        let playerInServer = player.discordSnowflake ? msg.guild.members.get(player.discordSnowflake) : undefined;
        let playerImg = playerInServer ? (playerInServer.user.avatarURL ? `${playerInServer.user.avatarURL}?size=128` : undefined) : undefined;
        let position = player.positionPrimary === "P" ? (player.rightHanded ? "RHP" : "LHP") : player.positionPrimary;
        let types = player.positionPrimary === "P" ? `${player.pitchingType.shortcode}-${player.pitchingBonus.shortcode}` : `${player.battingType.shortcode}-${player.rightHanded ? "R" : "L"}`;
        let embed = new Discord.RichEmbed({
            description: `${position} ${player.name} (${types}) has been drafted by the ${emoji ? emoji : ""}${team.name}.`,
            color: team.colorDiscord,
            footer: EMBED_FOOTER,
            author: {
                name: team.name,
                icon_url: `https://redditball.com/assets/images/teams/${team.tag}.png`,
            },
        });
        if (playerImg)
            embed.setThumbnail(playerImg);

        // Now send it
        let targetChannel = DEVELOPMENT_MODE ? msg.channel : (msg.guild.channels.get(SNOWFLAKE_CHANNEL_DRAFT) as Discord.TextChannel);
        await targetChannel.send(embed);

        if (targetChannel.id !== msg.channel.id)
            return "Done.";
        return undefined;
    }

}

class DraftTimerCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let userAuthorized = DEVELOPMENT_MODE || msg.channel.id === SNOWFLAKE_CHANNEL_OOTC;
        if (!userAuthorized)
            return `You can't do that.`;

        // Lookup the references
        let team = await getTeam(args.shift());

        // Make sure they're valid
        if (typeof team === "string")
            return team;

        if (draftTimerMessage) {
            await draftTimerMessage.delete();
            draftTimerMessage = undefined;
            draftTimerEnd = undefined;
            draftTimerTeam = undefined;
            draftTimerEmoji = undefined;
        }

        // Generate the embed
        let targetChannel = DEVELOPMENT_MODE ? msg.channel : (msg.guild.channels.get(SNOWFLAKE_CHANNEL_DRAFT) as Discord.TextChannel);
        let emoji = msg.guild.emojis.find(e => e.name.toLowerCase() === (team as Team).tag.toLowerCase());
        draftTimerEnd = Date.now() + 900000;
        draftTimerTeam = team;
        draftTimerEmoji = emoji;
        draftTimerMessage = await targetChannel.send(`${emoji ? emoji : ""}${team.tag} is picking. They have 15:00 remaining.`) as Discord.Message;

        if (targetChannel.id !== msg.channel.id)
            return "Done.";
        return undefined;
    }

}

setInterval(() => (async () => {
    if (draftTimerMessage) {
        let timeRemaining = (draftTimerEnd - Date.now()) / 1000;
        let trMins = Math.floor(Math.max(0, timeRemaining / 60));
        let trSecs = Math.floor(Math.max(0, timeRemaining % 60));
        let minsStr = trMins.toString().padStart(2, "0");
        let secsStr = trSecs.toString().padStart(2, "0");
        await draftTimerMessage.edit(`${draftTimerEmoji ? draftTimerEmoji : ""}${draftTimerTeam.tag} is picking. They have ${minsStr}:${secsStr} remaining.`);
        if (trMins === 0 && trSecs === 0) {
            draftTimerEnd = undefined;
            draftTimerMessage = undefined;
            draftTimerTeam = undefined;
        }
    }
})().catch(console.error), 5000);

export const DraftCommand = new DraftCommandClass();
export const DraftTimerCommand = new DraftTimerCommandClass();
