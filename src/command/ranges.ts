import {Message, RichEmbed} from "discord.js";
import {getUserSetting} from "../database";
import {PlayerSetting} from "../important-constants";
import {getPark, getPlayer} from "../site-pull";
import {calculateRanges} from "../SwingStat";
import {Command} from "./command";

/**
 * Provides swing ranges between two players on a specific field.
 */
class RangesCommandClass extends Command {

    description(): string {
        return "Calculates outcome ranges between a batter and a pitcher.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        // Undo the args split by spaces, and instead split by semicolon
        let argsT = args.join(" ").replace("@","@\u200b");
        args = argsT.split(";");

        // Make sure we have enough pieces
        if (args.length < 2) {
            return `\`.ranges batter; pitcher[; park]\``;
        }

        // Get the pieces
        let batterQuery = args[0].trim();
        let pitcherQuery = args[1].trim();
        let parkQuery = args[2];
        if (parkQuery === undefined)
            parkQuery = "Neutral Park";

        // Make sure they're all valid
        let batter = await getPlayer(batterQuery);
        if (typeof batter === "string") {
            return batter;
        }

        let pitcher = await getPlayer(pitcherQuery);
        if (typeof pitcher === "string") {
            return pitcher;
        }

        let park = await getPark(parkQuery.trim());
        if (typeof park === "string")
            return park;

        // Calculate range
        let calculatedRange = await calculateRanges(batter, pitcher, park);

        // Make the embed message format
        let description = "";
        if (pitcher.positionPrimary !== "P" && batter.positionPrimary === "P") // Maybe they got it backwards
            description += `**WARNING:** ${pitcher.name} is a position pitcher. Did you mean:\n\`.swing ${pitcherQuery}; ${batterQuery}; ${parkQuery}\`\n\n`;
        description += `${pitcher.name} pitching against ${batter.name} at ${park.name}.`;
        let ret: string | RichEmbed;

        if (await getUserSetting(msg.author.id, PlayerSetting.RangesCompact, false)) {
            ret = description + "\`\`\`";
            let outcomes = ["HR", "3B", "2B", "1B", "BB", "FO", "K", "PO", "RGO", "LGO"];
            for (let i = 0; i < outcomes.length; i++) {
                let start = i == 0 ? 0 : calculatedRange["range" + outcomes[i - 1]];
                let end = calculatedRange["range" + outcomes[i]] - 1;
                if (end - start < 0)
                    ret += `${outcomes[i].padStart(3, " ")}: ---\n`;
                else
                    ret += `${outcomes[i].padStart(3, " ")}: ${start.toString().padStart(3, "0")} - ${end.toString().padStart(3, "0")}\n`;
            }
            ret += "```";
        } else {
            ret = new RichEmbed({
                description,
                fields: [],
            });
            let outcomes = ["HR", "3B", "2B", "1B", "BB", "FO", "K", "PO", "RGO", "LGO"];
            for (let i = 0; i < outcomes.length; i++) {
                let start = i == 0 ? 0 : calculatedRange["range" + outcomes[i - 1]];
                let end = calculatedRange["range" + outcomes[i]] - 1;
                if (end - start < 0)
                    ret.addField(outcomes[i], "---", true);
                else
                    ret.addField(outcomes[i], `${start} - ${end}`, true);
            }
        }

        return ret;
    }

}

export const RangesCommand = new RangesCommandClass();
