import {Message} from "discord.js";
import * as rp from "request-promise";
import {getBotSetting} from "../database";
import {Command} from "./command";

class ScoreboardCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string> {
        let setting = await getBotSetting("scoreboard.session");
        let season = setting.split(".")[0];
        let session = setting.split(".")[1];

        let reqOptions = {
            uri: `https://redditball.com/api/v1/games/${season}/${session}`,
            json: true,
        };

        let games = await rp(reqOptions);

        let out = "```\n**MLR**\n---\nTEAMS|SCORE|INNING|BASES\n:--|:-:|:-:|:-:\n";

        let mlrGames = games.filter(g => !g.homeTeam.milr);
        for (let game of mlrGames) {
            out += `[${game.awayTeam.tag} @ ${game.homeTeam.tag}](https://redditball.com/games/${game.id})|${game.awayScore} - ${game.homeScore}|${game.completed ? `FINAL` : `${game.inning}.${game.outs}`}|${game.thirdOccupied ? "x" : "-"} ^${game.secondOccupied ? "x" : "-"} ${game.firstOccupied ? "x" : "-"}\n`;
        }

        out += "\n\n**MiLR**\n---\nTEAMS|SCORE|INNING|BASES\n:--|:-:|:-:|:-:\n";
        let milrGames = games.filter(g => g.homeTeam.milr);
        for (let game of milrGames) {
            out += `[${game.awayTeam.tag} @ ${game.homeTeam.tag}](https://redditball.com/games/${game.id})|${game.awayScore} - ${game.homeScore}|${game.completed ? `FINAL` : `${game.inning}.${game.outs}`}|${game.thirdOccupied ? "x" : "-"} ^${game.secondOccupied ? "x" : "-"} ${game.firstOccupied ? "x" : "-"}\n`;
        }

        out += "```";
        return out;
    }

}

export const ScoreboardCommand = new ScoreboardCommandClass();
