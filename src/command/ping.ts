import {Message, RichEmbed, TextChannel} from "discord.js";
import * as rp from "request-promise";
import {createNewABPing} from "../ab-ping-orchestrator";
import {getUserSetting} from "../database";
import {PlayerSetting, SNOWFLAKE_CHANNEL_AB_PINGS, SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN} from "../important-constants";
import {getPlayer, getTeam} from "../site-pull";
import {Command} from "./command";

/**
 * Notifies a player that it's their turn to bat.
 * Only accessible from the #umpires channel.
 */
class PingCommandClass extends Command {

    // Hide from other channels
    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        if (args.length < 1) {
            return `I need a player to ping.`;
        }
        let milrMode = false;
        if (args[0].toLowerCase() === "milr") {
            args = args.slice(1, args.length);
            milrMode = true;
        }
        return this.handlePingCommand(args, msg, milrMode);
    }

    // noinspection JSMethodCanBeStatic
    async handlePingCommand(args: string[], msg: Message, milrMode: boolean): Promise<string | RichEmbed> {
        // Make sure we have a player name to ping
        if (args.length < 1)
            return `I need a player to ping.`;

        // Find the player, or give them the search failure message if we couldn't find one
        let searchQuery = args.join(" ").replace("@","@\u200b");
        let player = await getPlayer(searchQuery);
        if (typeof player === "string")
            return player;

        // Let the ump know if we couldn't ping them at all
        if (!player.discordSnowflake)
            return `${player.name} isn't Discord Verified, so I can't ping them. :cry:`;

        if (await getUserSetting(player.discordSnowflake, PlayerSetting.PingOptIn) === false)
            return `${player.name} has opted out of receiving Discord pings for at-bats.`;

        // Create the base message
        let team = milrMode ? await getTeam(player.team.milrTeam) : player.team;
        if (typeof team === "string")
            return `I couldn't determine ${player.name}'s MiLR team.`;

        // Find the game link from the site
        let gameReq = team ? {
            uri: `https://redditball.com/api/v1/games/active/${team.tag}`,
            json: true,
        } : undefined;
        let game;
        try {
            game = gameReq ? await rp(gameReq) : undefined;
        } catch (ignored) {
            game = undefined;
        }

        let gameID36 = game ? game.id36 : undefined;

        let targetChannel =
            msg.guild.id === SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN ?
                msg.guild.channels.get(SNOWFLAKE_CHANNEL_AB_PINGS) as TextChannel :
                msg.guild.id === "593511953952145418" ?
                    msg.guild.channels.get("596774718510596102") as TextChannel :
                    msg.channel as TextChannel;

        // Store for message for when it's reacted to
        await createNewABPing(targetChannel, msg.channel as TextChannel, player.discordSnowflake, player.name, milrMode, [msg.author.id], gameID36);

        // Let the sender know if it wasn't sent to the same channel
        if (targetChannel.id !== msg.channel.id)
            return `Pinged ${player.name}!`;

        return undefined;
    }

}

export const PingCommand = new PingCommandClass();
