import {Message, RichEmbed} from "discord.js";
import {DEVELOPMENT_MODE, SNOWFLAKE_ROLE_COMMISSIONER, SNOWFLAKE_ROLE_TECH} from "../important-constants";
import {getPlayer} from "../site-pull";
import {Command} from "./command";

class LookupCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let userAuthorized = DEVELOPMENT_MODE || (msg.member && msg.member.roles.some((role, id) => id === SNOWFLAKE_ROLE_COMMISSIONER || id === SNOWFLAKE_ROLE_TECH));
        if (!userAuthorized)
            return "You can't do that.";

        let targetPlayerQ = args.join(" ");
        if (targetPlayerQ === undefined || targetPlayerQ.length === 0)
            return "You need to provide a player to lookup.";

        let player = await getPlayer(targetPlayerQ);
        if (typeof player === "string")
            return player;

        if (!player.discordSnowflake)
            return "This player is not Discord Verified. :cry:";

        return `<@${player.discordSnowflake}>`;
    }
}

export const LookupCommand = new LookupCommandClass();
