import * as Discord from "discord.js";
import { Message, RichEmbed, Snowflake, TextChannel, User } from "discord.js";
import {
    DEVELOPMENT_MODE,
    SNOWFLAKE_CHANNEL_OOTC,
    SNOWFLAKE_CHANNEL_PROPOSAL_VOTES,
    SNOWFLAKE_ROLE_COMMISSIONER, SNOWFLAKE_ROLE_TECH,
} from "../important-constants";
import { getPlayerByDiscord } from "../site-pull";
import { Command } from "./command";

class DiscordNamesCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let userAuthorized = DEVELOPMENT_MODE || msg.channel.id === SNOWFLAKE_CHANNEL_OOTC;
        if (!userAuthorized)
            return "You can't do that.";

        // Fetch list
        await msg.guild.fetchMembers();
        let members = msg.guild.members;

        // Create the counter message
        let newMsg = await msg.channel.send(`Please wait while I check accounts.... (0/${members.size})`) as Discord.Message;

        // Map to players
        let foundPlayers = [];
        let c = 0;
        for (let member of members.values()) {
            let player = await getPlayerByDiscord(member.user.id);
            if (typeof player !== "string")
                foundPlayers.push(`${player.id.toString().padStart(3, "0")}\t${member.user.username}#${member.user.discriminator}`);
            c++;
            if (c % 10 === 0)
                await newMsg.edit(`Please wait while I check accounts.... (${c}/${members.size})`);
        }

        await newMsg.edit(`Please wait while I check accounts.... (${c}/${members.size})`);

        // Compile to list
        foundPlayers = foundPlayers.sort();
        let fullStr = foundPlayers.join("\n");

        // Send attachment
        await msg.channel.send(new Discord.Attachment(Buffer.from(fullStr, "utf-8"), "names.tsv"));

        return undefined;
    }
}

export const DiscordNamesCommand = new DiscordNamesCommandClass();
