import {Message, RichEmbed} from "discord.js";
import {Command} from "./command";

class ClaimCommandClass extends Command {

    description(): string {
        return "Links your Discord account to your MLR player.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        return `The claim system has changed! Please visit https://redditball.com/account/me to link your Discord account.`;
    }
}

export const ClaimCommand = new ClaimCommandClass();
