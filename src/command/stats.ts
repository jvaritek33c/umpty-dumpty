import {GuildChannel, Message, RichEmbed} from "discord.js";
import {
    DEVELOPMENT_MODE,
    SNOWFLAKE_CHANNEL_BOT_COMMANDS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
} from "../important-constants";
import {getPlayer, getPlayer as getPlayerFromSite, getPlayerByDiscord, getPlayerWithStats} from "../site-pull";
import {Player} from "../site-types";
import {Command} from "./command";

async function generateBattingStatsMessage(player: Player): Promise<RichEmbed> {
    let hits = player.battingStats.totalHr + player.battingStats.total3b + player.battingStats.total2b + player.battingStats.total1b;
    let ob = hits + player.battingStats.totalBb;
    let ba = player.battingStats.totalAbs !== 0 ? (hits / player.battingStats.totalAbs) : 0;
    let obp = player.battingStats.totalPas !== 0 ? ob / player.battingStats.totalPas : 0;
    let slg = player.battingStats.totalAbs !== 0 ? (4 * player.battingStats.totalHr + 3 * player.battingStats.total3b + 2 * player.battingStats.total2b + player.battingStats.total1b) / player.battingStats.totalAbs : 0;
    return new RichEmbed({
        title: `Batting Stats for ${player.name} in Season ${player.battingStats.season}`,
        fields: [
            {
                name: "BA",
                value: ba.toFixed(3).substring(ba === 1 ? 0 : 1),
                inline: true,
            },
            {
                name: "OBP",
                value: obp.toFixed(3).substring(obp === 1 ? 0 : 1),
                inline: true,
            },
            {
                name: "SLG",
                value: slg.toFixed(3),
                inline: true,
            },
            {
                name: "OPS",
                value: (obp + slg).toFixed(3),
                inline: true,
            },
        ],
    });
}

async function generatePitchingStatsMessage(player: Player): Promise<RichEmbed> {
    let ipStr = Math.floor(player.pitchingStats.totalOuts / 3) + (0.1 * (player.pitchingStats.totalOuts % 3));
    let era = player.pitchingStats.totalEr === 0 ? 0 : player.pitchingStats.totalOuts === 0 ? Infinity : ((18 * player.pitchingStats.totalEr) / player.pitchingStats.totalOuts);
    let whip = player.pitchingStats.totalEr === 0 ? 0 : player.pitchingStats.totalOuts === 0 ? Infinity : (3 * (player.pitchingStats.totalHr + player.pitchingStats.total3b + player.pitchingStats.total2b + player.pitchingStats.total1b + player.pitchingStats.totalBb)) / player.pitchingStats.totalOuts;
    return new RichEmbed({
        title: `Pitching Stats for ${player.name} in Season ${player.pitchingStats.season}`,
        fields: [
            {
                name: "IP",
                value: ipStr.toFixed(1),
                inline: true,
            },
            {
                name: "ER",
                value: player.pitchingStats.totalEr.toString(),
                inline: true,
            },
            {
                name: "ERA",
                value: era === Infinity ? "Inf" : era.toFixed(2),
                inline: true,
            },
            {
                name: "WHIP",
                value: whip === Infinity ? "Inf" : whip.toFixed(3),
                inline: true,
            },
        ],
    });
}

class StatsCommandClass extends Command {

    private readonly pitching: boolean;
    private readonly season: number;
    private readonly milr: boolean;

    constructor(season: number, milr: boolean, pitching?: boolean) {
        super();
        this.season = season;
        this.milr = milr;
        this.pitching = pitching;
    }

    description(): string {
        return "Returns stats for a specific player.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let allowed = DEVELOPMENT_MODE ||
            !msg.guild ||
            msg.guild.id !== SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN ||
            msg.channel.id === SNOWFLAKE_CHANNEL_BOT_COMMANDS;
        if (!allowed)
            return undefined;

        let player: Player | string;
        let search = args.join(" ").trim().replace("@","@\u200b");
        if (search.length === 0)
            player = await getPlayerByDiscord(msg.author.id);
        else
            player = await getPlayerFromSite(search);

        if (typeof player === "string")
            return player;

        player = await getPlayerWithStats(player.id, this.season, this.milr);
        if (typeof player === "string")
            return player;

        if (this.pitching === undefined) {
            if (player.positionPrimary === "P" && player.pitchingStats)
                return generatePitchingStatsMessage(player);
            else if (player.battingStats)
                return generateBattingStatsMessage(player);
            else
                return "I don't have any stats for this player yet.";
        } else if (this.pitching && player.pitchingStats) {
            return generatePitchingStatsMessage(player);
        } else if (!this.pitching && player.battingStats) {
            return generateBattingStatsMessage(player);
        } else {
            return "I don't have any stats for this player yet.";
        }
    }
}

export const StatsCommand = new StatsCommandClass(4, false);
export const MiLRStatsCommand = new StatsCommandClass(4, true);
export const PStatsCommand = new StatsCommandClass(4, false, true);
export const BStatsCommand = new StatsCommandClass(4, false, false);
export const MiLRPStatsCommand = new StatsCommandClass(4, true, true);
export const MiLRBStatsCommand = new StatsCommandClass(4, true, false);
export const S3StatsCommand = new StatsCommandClass(3, false);
export const S3PStatsCommand = new StatsCommandClass(3, false, true);
export const S3BStatsCommand = new StatsCommandClass(3, false, false);
