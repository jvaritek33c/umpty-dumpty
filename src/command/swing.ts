import {RichEmbed} from "discord.js";
import {getPark, getPlayer} from "../site-pull";
import {calculateRanges} from "../SwingStat";
import {Command} from "./command";

/**
 * Calculates a swing result between two players on a specific field
 */
class SwingCommandClass extends Command {

    description(): string {
        return "Calculates swing outcomes between a batter and a pitcher.";
    }

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        // Undo the args split by spaces, and instead split by semicolon
        let argsT = args.join(" ").replace("@","@\u200b");
        args = argsT.split(";");

        // Make sure we have enough pieces
        if (args.length < 4) {
            return `\`.swing batter; swing; pitcher; pitch[; park]\``;
        }

        // Get the pieces
        let batterQuery = args[0].trim();
        let swingNum = parseInt(args[1]);
        let pitcherQuery = args[2].trim();
        let pitchNum = parseInt(args[3]);
        let parkQuery = args[4];
        if (parkQuery === undefined)
            parkQuery = "Neutral Park";

        // Make sure they're all valid
        if (pitchNum === undefined || pitchNum < 1 || pitchNum > 1000) {
            return `The pitch needs to be a number between 1 and 1000.`;
        }

        if (swingNum === undefined || swingNum < 1 || swingNum > 1000) {
            return `The swing needs to be a number between 1 and 1000.`;
        }

        let pitcher = await getPlayer(pitcherQuery);
        if (typeof pitcher === "string") {
            return pitcher;
        }

        let batter = await getPlayer(batterQuery);
        if (typeof batter === "string") {
            return batter;
        }

        let park = await getPark(parkQuery.trim());
        if (typeof park === "string")
            return park;

        // Calculate range and diff
        let calculatedRange = await calculateRanges(batter, pitcher, park);
        let diff = getDifference(pitchNum, swingNum);

        // Finally, find diff along ranges to get result
        let outcomes = ["HR", "3B", "2B", "1B", "BB", "FO", "K", "PO", "RGO", "LGO"];
        let outcome = "";
        for (let o of outcomes) {
            if (diff < calculatedRange["range" + o]) {
                outcome = o;
                break;
            }
        }

        // Create the message to send back
        let ret = ``;
        if (pitcher.positionPrimary !== "P" && batter.positionPrimary === "P") // Maybe they got it backwards
            ret += `**WARNING:** ${pitcher.name} is a position pitcher. Did you mean:\n\`.swing ${pitcherQuery}; ${pitchNum}; ${batterQuery}; ${swingNum}; ${parkQuery}\``;
        ret += `\n\n${pitcher.name} pitching against ${batter.name} at ${park.name}.`;
        ret += `\n\nPitch: ${pitchNum}  \nSwing: ${swingNum}  \nDiff: ${diff} -> ${outcome}`;

        return ret;
    }
}

function getDifference(pitch, swing): number {
    let max = Math.max(pitch, swing);
    let min = Math.min(pitch, swing);
    return Math.min(max - min, 1000 - max + min);
}

export const SwingCommand = new SwingCommandClass();
