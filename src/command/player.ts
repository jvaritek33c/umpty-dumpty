import * as Discord from "discord.js";
import {RichEmbed} from "discord.js";
import {getPlayer as getPlayerFromSite, getPlayerByDiscord} from "../site-pull";
import {Player} from "../site-types";
import {Command} from "./command";

/**
 * Shows player info in a neat little embed card
 */
class PlayerCommandClass extends Command {

    description(): string {
        return "Provides player information for any player in the league.";
    }

    public async handleCommand(args: string[], msg: Discord.Message): Promise<string | RichEmbed> {
        let player: Player | string;
        let search = args.join(" ").trim().replace("@","@\u200b");
        if (search.length === 0)
            player = await getPlayerByDiscord(msg.author.id);
        else
            player = await getPlayerFromSite(search);

        if (typeof player === "string")
            return player;

        // Now that we have a player, we create the embed fields with all their info
        let fields = [];
        fields.push({
            inline: true,
            name: `Discord Name`,
            value: player.discordSnowflake ? `<@${player.discordSnowflake}>` : "No Discord",
        });
        fields.push({inline: true, name: `Reddit Name`, value: player.redditName});
        fields.push({inline: true, name: `Primary Position`, value: player.positionPrimary});
        fields.push({inline: true, name: `Secondary Position`, value: player.positionSecondary ? player.positionSecondary : "None"});
        fields.push({inline: true, name: `Extra Position`, value: player.positionTertiary ? player.positionTertiary : "None"});
        fields.push({inline: true, name: `Team`, value: player.team ? player.team.tag : "FA"});
        fields.push({inline: true, name: `Batting Type`, value: player.battingType.name});
        fields.push({inline: true, name: `Hand`, value: player.rightHanded ? "Right" : "Left"});
        if (player.pitchingType) {
            fields.push({inline: true, name: `Pitching Type`, value: player.pitchingType.name});
            fields.push({inline: true, name: `Pitching Bonus`, value: player.pitchingBonus.name});
        }

        // Now create the embed and send it
        return new Discord.RichEmbed({
            title: player.name,
            color: player.team ? player.team.colorDiscord : 0xFFFFFF,
            thumbnail: { url: `https://redditball.duckblade.com/assets/images/teams/${player.team ? player.team.tag : ""}.png` },
            // description: `**Discord Name:** ${searchResult.discordName.length > 0 ? searchResult.discordName : "No Discord"}\n**Reddit Name:** ${searchResult.redditName}\n**Team:** ${searchResult.team.tag}\n**Batting Type:** ${searchResult.battingType.name}\n**Pitching Type:** ${searchResult.pitchingType.name}\n**Pitching Bonus:** ${searchResult.pitchingBonus.name}`,
            fields,
        });
    }
}

export const PlayerCommand = new PlayerCommandClass();
