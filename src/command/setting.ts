import { Message, RichEmbed } from "discord.js";
import { setUserSetting } from "../database";
import { PlayerSetting } from "../important-constants";
import { getPark, getPlayer } from "../site-pull";
import { calculateRanges } from "../SwingStat";
import { Command } from "./command";

/**
 * Allows players to set personal settings for their use of the bot.
 */
class SettingCommandClass extends Command {

    description(): string {
        return "Calculates outcome ranges between a batter and a pitcher.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let settingKey = args.shift() as PlayerSetting;
        let settingValue = args.shift();

        if (!settingKey || !settingValue || !Object.values(PlayerSetting).includes(settingKey) || (settingValue !== "true" && settingValue !== "false")) {
            let allSettings = "";
            for (let setting in PlayerSetting) {
                if (PlayerSetting[setting])
                    allSettings += PlayerSetting[setting] + "\n";
            }
            return `You must provide a setting name and a value. Valid settings are:\n\`\`\`\n${allSettings}\`\`\`\nValid values are:\n\`\`\`\ntrue\nfalse\`\`\``;
        }

        await setUserSetting(msg.author.id, settingKey, settingValue === "true");
        return "Done.";
    }

}

export const SettingCommand = new SettingCommandClass();
