import {Message} from "discord.js";
import {Command} from "./command";

class PollCommandClass extends Command {

    async handleCommand(args: string[], msg: Message): Promise<undefined> {
        // Send the message
        let content = args.join(" ").replace("@","@\u200b");
        let message = await msg.channel.send(`Poll from <@${msg.author.id}>:\n${content}`) as Message;

        // Delete the command one if possible
        if (msg.deletable)
            await msg.delete(3000);

        // Add the reactions
        await message.react("👍");
        await message.react("👎");

        return undefined;
    }

}

export const PollCommand = new PollCommandClass();
