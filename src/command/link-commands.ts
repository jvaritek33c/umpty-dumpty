import {RichEmbed} from "discord.js";
import {Command} from "./command";

class HandbookCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        return `Here's the current MLR Umpire Handbook!\n\nhttps://docs.google.com/document/d/1XbUMvwiG-7hAPD0ZfPKaqOc0l0rj5a7_oVzTXhwDTWc`;
    }
}

class RulebookCommandClass extends Command {

    description(): string {
        return "Provides a link for the current MLR rulebook.";
    }

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        return `Here's the current MLR Rulebook!\n\nhttps://fakebaseball.gitlab.io/rulebook/rulebook.pdf`;
    }

}

export const HandbookCommand = new HandbookCommandClass();
export const RulebookCommand = new RulebookCommandClass();
