import {Message, RichEmbed, TextChannel} from "discord.js";
import {addComplaint} from "../database";
import {
    DEVELOPMENT_MODE,
    SNOWFLAKE_CHANNEL_COMPLAINTS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
} from "../important-constants";
import {Command} from "./command";

const BLACKLIST = [
    "86530663963983872", // sael
    "666878974324113409", //notsael
    "439595920607412244", //saelsaelsael
    "666886469528322058", //aslkdfaksdflk (another probable sael alt)
];

class ComplaintCommandClass extends Command {

    description(): string {
        return "Files an anonymous complaint for the OOTC to review.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        if (BLACKLIST.includes(msg.author.id)) {
            return "You can't do that.";
        }

        let content = args.join(" ").trim().replace("@","@\u200b");
        if (content.length < 1) {
            return `\`.complaint my complaint here\``;
        }
        content = `VIA ANONYMOUS:\n${content}`;

        if (content.length > 1900) {
            return `Please shorten your message to < 1900 characters, and use .creply to append more information if necessary.`;
        }

        let id = await addComplaint(msg.author.id, content);

        content = `COMPLAINT ${id} ${content}`;

        if (DEVELOPMENT_MODE) {
            await msg.channel.send(content);
        } else {
            await (msg.client.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).channels.get(SNOWFLAKE_CHANNEL_COMPLAINTS) as TextChannel).send(content);
        }

        return `I've submitted your complaint to the OOTC with ID ${id}. You will receive a message if a reply is warranted.\n\nTo include further information to this complaint, please use \`.creply ${id} my content here\``;
    }
}

export const ComplaintCommand = new ComplaintCommandClass();
