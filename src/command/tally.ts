import * as Discord from "discord.js";
import {Message, RichEmbed, Snowflake, TextChannel, User} from "discord.js";
import {
    DEVELOPMENT_MODE,
    SNOWFLAKE_CHANNEL_OOTC,
    SNOWFLAKE_CHANNEL_PROPOSAL_VOTES,
    SNOWFLAKE_ROLE_COMMISSIONER, SNOWFLAKE_ROLE_TECH,
} from "../important-constants";
import {getPlayerByDiscord} from "../site-pull";
import {Command} from "./command";

class TallyCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let userAuthorized = DEVELOPMENT_MODE || msg.channel.id === SNOWFLAKE_CHANNEL_OOTC;
        if (!userAuthorized)
            return "You can't do that.";

        // Get the snowflake
        let voteMsgId = args.shift() as Snowflake;
        if (voteMsgId === undefined)
            return "Please provide the ID of the committee vote to tally.";

        let responseMessage = (await msg.channel.send("Please wait while I tally the results....")) as Discord.Message;

        // Grab the reaction options from the message
        let positiveReactionT = args.shift();
        let positiveReaction = positiveReactionT ? (positiveReactionT.startsWith("<") ? positiveReactionT.substring(2, positiveReactionT.length - 1) : positiveReactionT) : "👍";
        let negativeReactionT = args.shift();
        let negativeReaction = negativeReactionT ? (negativeReactionT.startsWith("<") ? negativeReactionT.substring(2, negativeReactionT.length - 1) : negativeReactionT) : "👎";
        let abstainReactionT = args.shift();
        let abstainReaction = abstainReactionT ? (abstainReactionT.startsWith("<") ? abstainReactionT.substring(2, abstainReactionT.length - 1) : abstainReactionT) : "😶";

        // Make sure it's valid
        let channel = DEVELOPMENT_MODE ? msg.channel : (msg.guild.channels.get(SNOWFLAKE_CHANNEL_PROPOSAL_VOTES) as TextChannel);
        let voteMsg: Message = await channel.fetchMessage(voteMsgId);
        if (!voteMsg)
            return `I couldn't find that vote in the <#${SNOWFLAKE_CHANNEL_PROPOSAL_VOTES}> channel.`;

        // Get the reactions
        let posReactions = voteMsg.reactions.get(positiveReaction);
        let posReactors: User[] = posReactions ? (await posReactions.fetchUsers()).array() : [];
        let negReactions = voteMsg.reactions.get(negativeReaction);
        let negReactors: User[] = negReactions ? (await negReactions.fetchUsers()).array() : [];
        let absReactions = voteMsg.reactions.get(abstainReaction);
        let absReactors: User[] = negReactions ? (await absReactions.fetchUsers()).array() : [];

        // Figure out all the teams of the voters and compile it into a list
        let posTeams: string[] = [];
        let negTeams: string[] = [];
        let absTeams: string[] = [];
        let uncertain: string[] = [];
        for (let reactor of posReactors) {
            let player = await getPlayerByDiscord(reactor.id);
            let member = await msg.guild.fetchMember(reactor.id);
            if (typeof player === "string")
                uncertain.push(`${reactor.username}#${reactor.discriminator}`);
            else {
                if (member.roles.some(role => role.id === SNOWFLAKE_ROLE_COMMISSIONER || role.id === SNOWFLAKE_ROLE_TECH)) {
                    posTeams.push(`OOTC: ${player.name}`);
                } else {
                    let team = player.team.tag;
                    let index = posTeams.indexOf(team);
                    if (index !== -1)
                        posTeams[index] += " x2";
                    else
                        posTeams.push(team);
                }
            }
        }
        for (let reactor of negReactors) {
            let player = await getPlayerByDiscord(reactor.id);
            let member = await msg.guild.fetchMember(reactor.id);
            if (typeof player === "string")
                uncertain.push(`${reactor.username}#${reactor.discriminator}`);
            else {
                if (member.roles.some(role => role.id === SNOWFLAKE_ROLE_COMMISSIONER || role.id === SNOWFLAKE_ROLE_TECH)) {
                    let pIndex = posTeams.indexOf(`OOTC: ${player.name}`);
                    if (pIndex === -1)
                        negTeams.push(`OOTC: ${player.name}`);
                    else {
                        posTeams[pIndex] += " CONFLICT";
                        negTeams.push(`OOTC: ${player.name} CONFLICT`);
                    }
                } else {
                    let team = player.team.tag;
                    let nIndex = negTeams.indexOf(team);
                    if (nIndex !== -1)
                        negTeams[nIndex] += " x2";
                    else
                        nIndex = negTeams.push(team) - 1;
                    let pIndex = posTeams.indexOf(team);
                    pIndex = pIndex !== -1 ? pIndex : posTeams.indexOf(team + " x2");
                    if (pIndex !== -1) {
                        posTeams[pIndex] += " CONFLICT";
                        negTeams[nIndex] += " CONFLICT";
                    }
                }
            }
        }
        for (let reactor of absReactors) {
            let player = await getPlayerByDiscord(reactor.id);
            let member = await msg.guild.fetchMember(reactor.id);
            if (typeof player === "string")
                uncertain.push(`${reactor.username}#${reactor.discriminator}`);
            else {
                if (member.roles.some(role => role.id === SNOWFLAKE_ROLE_COMMISSIONER || role.id === SNOWFLAKE_ROLE_TECH)) {
                    let pIndex = posTeams.indexOf(`OOTC: ${player.name}`);
                    let nIndex = negTeams.indexOf(`OOTC: ${player.name}`);
                    if (pIndex === -1 && nIndex === -1)
                        absTeams.push(`OOTC: ${player.name}`);
                    else {
                        if (pIndex !== -1)
                            posTeams[pIndex] += " CONFLICT";
                        if (nIndex !== -1)
                            negTeams[nIndex] += " CONFLICT";
                        absTeams.push(`OOTC: ${player.name} CONFLICT`);
                    }
                } else {
                    let team = player.team.tag;
                    let aIndex = absTeams.indexOf(team);
                    if (aIndex !== -1)
                        absTeams[aIndex] += " x2";
                    else
                        aIndex = absTeams.push(team) - 1;
                    let pIndex = posTeams.indexOf(team);
                    let nIndex = negTeams.indexOf(team);
                    pIndex = pIndex !== -1 ? pIndex : posTeams.indexOf(team + " x2");
                    nIndex = nIndex !== -1 ? nIndex : negTeams.indexOf(team + " x2");
                    if (pIndex !== -1 || nIndex !== -1) {
                            absTeams[aIndex] += " CONFLICT";
                            if (pIndex !== -1)
                            posTeams[pIndex] += " CONFLICT";
                            if (nIndex !== -1)
                            negTeams[pIndex] += " CONFLICT";
                    }
                }
            }
        }

        // Plop the message in
        await responseMessage.edit(`FOR: ${posTeams.length}\`\`\`\n${posTeams.length === 0 ? "None" : posTeams.sort((a, b) => a.localeCompare(b)).join("\n")}\`\`\`AGAINST: ${negTeams.length}\`\`\`\n${negTeams.length === 0 ? "None" : negTeams.sort((a, b) => a.localeCompare(b)).join("\n")}\`\`\`ABSTAIN: ${absTeams.length}\`\`\`\n${absTeams.length === 0 ? "None" : absTeams.sort((a, b) => a.localeCompare(b)).join("\n")}\`\`\`${uncertain.length === 0 ? "" : `UNIDENTIFIED:\`\`\`\n${uncertain.sort((a, b) => a.localeCompare(b)).join("\n")}\`\`\``}Approval Rate: ${((posTeams.length * 100) / (posTeams.length + negTeams.length)).toFixed(1)}%`);

        // Don't return anything, to not make a new message
        return undefined;
    }

}

export const TallyCommand = new TallyCommandClass();
