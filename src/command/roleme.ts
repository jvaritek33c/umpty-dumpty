import {Message, RichEmbed} from "discord.js";
import {SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN} from "../important-constants";
import {getPlayerByDiscord} from "../site-pull";
import {Command} from "./command";

const roles = {
    FA: "416390298961313792",
    ARI: "346462278767607828",
    ATL: "592802862355185721",
    BAL: "592808285321822209",
    BOS: "377983676765437952",
    CHC: "490677176090361866",
    CWS: "592801636976427018",
    CIN: "378608070802014208",
    CLE: "593810843070889985",
    COL: "377970664671412234",
    DET: "346462526193664000",
    HOU: "346461790495834113",
    KCR: "490679938660171776",
    LAA: "490680077609074698",
    LAD: "379050482259329027",
    MIA: "490678719212683265",
    MIL: "346462375651704832",
    MIN: "592802159704277002",
    MTL: "346462881321320458",
    NYM: "490648079704522753",
    NYY: "496478067200622602",
    OAK: "346461698242379786",
    PHI: "346462550101327874",
    PIT: "346461892899897346",
    SDP: "378018435235315724",
    SFG: "378976251161083906",
    SEA: "592805613139132426",
    STL: "377990464197885952",
    TBD: "377998834401411073",
    TEX: "378660297595944960",
    TOR: "346461541400444968",
};

const milrRoles = {
    ARI: "600873436222586880",
    ATL: "600873424067493888",
    BAL: "600873422016479301",
    BOS: "600873424067493888",
    CHC: "600873429696118791",
    CWS: "600873431109599237",
    CIN: "600873432841715742",
    CLE: "600873432841715742",
    COL: "600873437564764173",
    DET: "600873429696118791",
    HOU: "600873441205420032",
    KCR: "600873426428887041",
    LAA: "600873437564764173",
    LAD: "600873441205420032",
    MIA: "600873416211562497",
    MIL: "600873426428887041",
    MIN: "600873427368149004",
    MTL: "600873413023760389",
    NYM: "600873422016479301",
    NYY: "600873416211562497",
    OAK: "600873436222586880",
    PHI: "600873419755487242",
    PIT: "600873427368149004",
    SDP: "600873439246417931",
    SFG: "600873434246938625",
    SEA: "600873439246417931",
    STL: "600873431109599237",
    TBD: "600873419755487242",
    TEX: "600873434246938625",
    TOR: "600873413023760389",
};

let allRoles = Object.keys(roles).map(key => roles[key]);
let allMilrRoles = Object.keys(milrRoles).map(key => milrRoles[key]);

class RoleMeCommandClass extends Command {

    description(): string {
        return "Fixes your roles in the r/fakebaseball server.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        if (!msg.member || msg.guild.id !== SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN) {
            return "This command can only be run in the r/fakebaseball server.";
        }

        // Get the player
        let player = await getPlayerByDiscord(msg.author.id);
        if (typeof player === "string")
            return player;

        // Update roles
        let roleToAssign = player.team ? roles[player.team.tag] : roles.FA;
        await msg.member.removeRoles(allRoles);
        await msg.member.addRole(roleToAssign);

        return "Done.";
    }
}

class RoleMeMiLRCommandClass extends Command {

    description(): string {
        return "Fixes your roles in the r/fakebaseball server.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        if (!msg.member || msg.guild.id !== SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN) {
            return "This command can only be run in the r/fakebaseball server.";
        }

        // Get the player
        let player = await getPlayerByDiscord(msg.author.id);
        if (typeof player === "string")
            return player;

        // Update roles
        let roleToAssign = player.team ? milrRoles[player.team.tag] : undefined;
        if (roleToAssign) {
            await msg.member.removeRoles(allMilrRoles);
            await msg.member.addRole(roleToAssign);
        }

        return "Done.";
    }
}

export const RoleMeCommand = new RoleMeCommandClass();
export const RoleMeMiLRCommand = new RoleMeMiLRCommandClass();
