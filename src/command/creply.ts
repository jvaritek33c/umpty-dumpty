import {Message, RichEmbed, TextChannel} from "discord.js";
import {addComplaint, appendComplaint, getComplaintAuthor} from "../database";
import {SNOWFLAKE_CHANNEL_COMPLAINTS, SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN} from "../important-constants";
import {Command} from "./command";

class ComplaintReplyCommandClass extends Command {

    description(): string {
        return "Files an anonymous complaint for the OOTC to review.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let complaintId = parseInt(args.shift());

        if (!isFinite(complaintId)) {
            return `\`.creply <id> my complaint here\``;
        }

        let content = args.join(" ").trim();
        if (content.length < 1) {
            return `\`.creply <id> my complaint here\``;
        }

        let origAuthor = await getComplaintAuthor(complaintId);
        if (msg.channel.id === SNOWFLAKE_CHANNEL_COMPLAINTS) {
            content = `COMPLAINT ${complaintId} VIA OOTC:\n${content}`;
        } else {
            if (msg.author.id !== origAuthor) {
                return `This is not a valid complaint id.`;
            }

            content = `COMPLAINT ${complaintId} VIA ANONYMOUS:\n${content}`;
        }

        if (content.length > 1900) {
            return `Please shorten your message to < 1900 characters, and use .creply to append more information if necessary.`;
        }

        await appendComplaint(complaintId, content);

        if (msg.channel.id === SNOWFLAKE_CHANNEL_COMPLAINTS) {
            let author = await msg.client.fetchUser(origAuthor);
            if (author) {
                await author.send(content);
            }
        } else {
            await (msg.client.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).channels.get(SNOWFLAKE_CHANNEL_COMPLAINTS) as TextChannel).send(content);
        }

        return `I've appended more information to complaint ${complaintId} and notified ${msg.channel.id === SNOWFLAKE_CHANNEL_COMPLAINTS ? "the original author" : "the OOTC"}.`;
    }
}

export const ComplaintReplyCommand = new ComplaintReplyCommandClass();
