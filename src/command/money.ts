import {Message, RichEmbed} from "discord.js";
import {Command} from "./command";

class MoneyCommandClass extends Command {

    description(): string {
        return "";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        return `MTZKS0d3MGI0RlBRSTFhZm8yLUhoRF9NYm1mdS1WQ1F5SUxDUElidUhTMnM=`;
    }
}

export const MoneyCommand = new MoneyCommandClass();
