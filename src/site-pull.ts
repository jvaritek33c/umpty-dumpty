import * as Discord from "discord.js";
import * as rp from "request-promise";
import {Park, Player, Team} from "./site-types";
import {Named} from "./SwingStat";

async function searchEndpoint<T>(endpoint: string): Promise<T[] | string> {
    let options = {
        uri: endpoint,
        json: true,
    };
    try {
        return await rp(options) as T[];
    } catch (e) {
        return `Something went wrong when contacting the site. Please try again later.`;
    }
}

function namePredicate<T extends Named>(obj: T, query: string): boolean {
    return obj.name.toLowerCase() === query.toLowerCase();
}

function filterResult<T extends Named>(results: T[] | string, query: string, predicate: ((obj: T, query: string) => boolean) = namePredicate): T | string {
    if (typeof results === "string")
        return results;
    if (results.length === 1)
        return results[0];
    else if (results.length === 0)
        return `Your search for ${query} yielded no results.`;
    else {
        let exactMatch = results.find(r => predicate(r, query));
        if (exactMatch)
            return exactMatch;

        if (results.length < 10)
            return `Your search for ${query} returned too many results:\n${results.map(r => r.name).join("\n")}`;
        else
            return `Your search for ${query} returned too many results. Please be more specific.`;
    }
}

export async function searchPlayers(query: string): Promise<Player[] | string> {
    return await searchEndpoint<Player>(`https://redditball.com/api/v1/players/search?query=${query}`);
}

export async function getPlayer(query: string): Promise<string | Player> {
    return filterResult(await searchPlayers(query), query, (p, q) => p.name.toLowerCase() === q.toLowerCase() || p.redditName.toLowerCase() === q.toLowerCase());
}

export async function searchTeams(search: string): Promise<Team[] | string> {
    return await searchEndpoint<Team>(`https://redditball.com/api/v1/teams/search?query=${search}`);
}

export async function getTeam(query: string | number): Promise<string | Team> {
    if (typeof query === "number") {
        try {
            return await rp({uri: `https://redditball.com/api/v1/teams/${query}`, json: true}) as Team;
        } catch (e) {
            return `Something went wrong when contacting the site. Please try again later.`;
        }
    } else {
        return filterResult(await searchTeams(query), query.toString(), (t, q) => t.name.toLowerCase() === q.toLowerCase() || t.tag.toLowerCase() === q.toLowerCase());
    }
}

export async function searchParks(query: string): Promise<Park[] | string> {
    return await searchEndpoint<Park>(`https://redditball.com/api/v1/parks/search?query=${query}`);
}

export async function getPark(query: string): Promise<string | Park> {
    return filterResult(await searchParks(query), query);
}

export async function getPlayerByDiscord(snowflake: Discord.Snowflake): Promise<Player | string> {
    let options = {
        uri: `https://redditball.com/api/v1/players/byDiscord?snowflake=${snowflake}`,
        json: true,
    };
    try {
        return await rp(options) as Player;
    } catch (e) {
        return `I wasn't able to find your player. Link it at https://redditball.com/account/me`;
    }
}

export async function getPlayerWithStats(id: number, season: number = 4, milr: boolean = false): Promise<Player | string> {
    let options = {
        uri: `https://redditball.com/api/v1/players/${id}/stats?season=${season}&milr=${milr}`,
        json: true,
    };
    try {
        return await rp(options) as Player;
    } catch (e) {
        return `Something went wrong contacting the site. Please try again later.`;
    }
}
