import {Message, Snowflake, TextChannel, User} from "discord.js";
import {getUserSetting} from "./database";
import {PlayerSetting} from "./important-constants";

class PendingReaction {
    message: Message;
    pingedUser: string;
    pingedSnowflake: Snowflake;
    srcChannel: TextChannel;
    pingers: Snowflake[];

    constructor(message: Message, pingedUser: string, pingedSnowflake: Snowflake, srcChannel: TextChannel, pingers: Snowflake[]) {
        this.message = message;
        this.pingedUser = pingedUser;
        this.pingedSnowflake = pingedSnowflake;
        this.srcChannel = srcChannel;
        this.pingers = pingers;
    }
}

const pendingReactions: PendingReaction[] = [];

export async function createNewABPing(channel: TextChannel, responseChannel: TextChannel, target: Snowflake, name: string, milr: boolean, pingers: Snowflake[], id36: string) {
    let pingMessage = `<@${target}>, you're up to bat${milr ? " in MiLR" : ""}!`;
    if (id36)
        pingMessage += `\n\nhttps://reddit.com/r/fakebaseball/comments/${id36}/`;
    pingMessage += `\n\nTap the baseball below to let the umpire know you've swung.`;

    let createdMessage = (await channel.send(pingMessage)) as Message;
    await createdMessage.react("⚾");

    registerNewPing(createdMessage, name, target, responseChannel, pingers);
}

export function registerNewPing(message: Message, pingedUser: string, pingedSnowflake: Snowflake, srcChannel: TextChannel, pingers: Snowflake[]) {
    pendingReactions.push(new PendingReaction(message, pingedUser, pingedSnowflake, srcChannel, pingers));
}

export async function handlePingReaction(reactedMsg: Message, user: User) {
    for (let [index, pending] of pendingReactions.entries()) {
        if (pending.message.id === reactedMsg.id && pending.pingedSnowflake === user.id) {
            reactedMsg.react("👍")
                .catch(console.error);
            pendingReactions.splice(index, 1);
            let targets: string[] = [];
            for (let pinger of pending.pingers) {
                if (await getUserSetting(pinger, PlayerSetting.PingOptInUmp, true))
                    targets.push(`<@${pinger}>, `);
            }
            await pending.srcChannel.send(`${targets.join("")}${pending.pingedUser} has swung!`);
            break;
        }
    }
}
