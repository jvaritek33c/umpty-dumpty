import {Named, SwingStat} from "./SwingStat";

export class Park extends Named {
    id: number;
    name: string;
    factorHR: number;
    factor3B: number;
    factor2B: number;
    factor1B: number;
    factorBB: number;
}

export class Team extends Named {
    id: number;
    name: string;
    tag: string;
    park: number | Park;
    colorDiscord: number;
    colorRoster: number;
    colorRosterBG: number;
    discordRole: string;
    milr: boolean;
    milrTeam: number;
}

export class BattingType extends SwingStat {
    id: number;
    shortcode: string;
}

export class PitchingType extends SwingStat {
    id: number;
    shortcode: string;
}

export class PitchingBonus extends SwingStat {
    id: number;
    shortcode: string;
}

export class BattingStats {
    season: number;
    milr: boolean;
    totalPas: number;
    totalAbs: number;
    totalHr: number;
    total3b: number;
    total2b: number;
    total1b: number;
    totalBb: number;
    totalFo: number;
    totalK: number;
    totalPo: number;
    totalRgo: number;
    totalLgo: number;
    totalRbi: number;
    totalR: number;
    totalSb: number;
    totalCs: number;
    totalDp: number;
    totalTp: number;
    totalGp: number;
    totalSac: number;
}

export class PitchingStats {
    season: number;
    milr: boolean;
    totalPas: number;
    totalOuts: number;
    totalEr: number;
    totalHr: number;
    total3b: number;
    total2b: number;
    total1b: number;
    totalBb: number;
    totalFo: number;
    totalK: number;
    totalPo: number;
    totalRgo: number;
    totalLgo: number;
    totalSb: number;
    totalCs: number;
    totalDp: number;
    totalTp: number;
    totalGp: number;
    totalSac: number;
}

export class Player extends Named {
    id: number;
    name: string;
    redditName: string;
    discordSnowflake: string;
    team: Team;
    battingType: BattingType;
    pitchingType: PitchingType;
    pitchingBonus: PitchingBonus;
    rightHanded: boolean;
    positionPrimary: string;
    positionSecondary: string;
    positionTertiary: string;
    battingStats: BattingStats;
    pitchingStats: PitchingStats;
}
