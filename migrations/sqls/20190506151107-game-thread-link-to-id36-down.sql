ALTER TABLE games
    CHANGE COLUMN id36 thread_link VARCHAR(150);

UPDATE games SET thread_link=CONCAT('https://reddit.com/r/fakebaseball/comments/', thread_link, '/');
