create table complaints (
    id int not null auto_increment primary key,
    author varchar(18) not null,
    content text not null
);
