create table if not exists people
(
    person_snowflake varchar(32) not null
        primary key,
    reddit_name varchar(64) not null,
    claim_method varchar(32) default 'Unknown' not null,
    claim_time timestamp default CURRENT_TIMESTAMP not null
);

# A few people for testing

INSERT INTO people (person_snowflake, reddit_name, claim_method, claim_time) VALUES ('138058881090715648', '/u/zamboneh', 'TheClaiming2', '2019-04-27 01:14:43') ON DUPLICATE KEY UPDATE claim_method=claim_method;
INSERT INTO people (person_snowflake, reddit_name, claim_method, claim_time) VALUES ('138318226181324800', '/u/llamositopia', 'Early', '2019-04-25 04:38:24') ON DUPLICATE KEY UPDATE claim_method=claim_method;
INSERT INTO people (person_snowflake, reddit_name, claim_method, claim_time) VALUES ('140306370359459840', '/u/mvh1015', 'Early', '2019-04-25 04:46:05') ON DUPLICATE KEY UPDATE claim_method=claim_method;
INSERT INTO people (person_snowflake, reddit_name, claim_method, claim_time) VALUES ('174593610052927488', '/u/xboy623', 'TheClaiming', '2019-04-25 06:03:20') ON DUPLICATE KEY UPDATE claim_method=claim_method;
INSERT INTO people (person_snowflake, reddit_name, claim_method, claim_time) VALUES ('267010638079852544', '/u/snicklefritzed', 'Early', '2019-04-25 04:38:24') ON DUPLICATE KEY UPDATE claim_method=claim_method;
